# Chad Lavimoniere

Hiya, I'm Chad. I'm a product designer and life-long learner based in Brooklyn, NY.

When I'm not at work, some of the things I'm learning about these days are how to play the violin, how to make great backyard pizza, growing a pollinator garden, watercolor painting, and keeping an urban sketch notebook.

<figure>
<img src="https://gitlab.com/chadlavimoniere/chadlavimoniere/-/raw/main/chad-about-me.jpeg" height="962" width="1280" />
<figcaption>(I'm the one on the right, in the hat)</figcaption>
</figure>

(I'm also https://gitlab.com/clavimoniere)
